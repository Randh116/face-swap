from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from bs4 import BeautifulSoup
import time
import requests
import shutil
import os
import urllib.request as urllib2
from path_constants import PATH_FACES_REC
import tkinter as tk


class ImageDownloader(object):
    _URL = 'https://www.google.com/search?q={}&source=lnms&tbm=isch&sa=X&ved=2ahUKEwie44_AnqLpAhUhBWMBHUFGD90Q_AUoAXoECBUQAw&biw=1920&bih=947'
    _BASE_DIR = PATH_FACES_REC
    _OPTIONS = Options()
    _OPTIONS.headless = True
    _DRIVER = webdriver.Firefox(executable_path='D:/moje/studia_3rok/sys_wiz/face-swap/data/search/geckodriver.exe', options=_OPTIONS)
    _HEADERS = {
        'User-Agent': "Mozilla/5.0 (Windows NT 6.1; WOW64) "
                      "AppleWebKit/537.36 (KHTML, like Gecko) "
                      "Chrome/43.0.2357.134 "
                      "Safari/537.36"
    }

    def __init__(self, query="marcin gortat"):
        # if query is None:
        #    query = "Adam Malysz"
        self.dirName = self.__createDirName(query)
        self.dirPath = self.__createDirPath()
        query = self.__createQuery(query)
        self.url = self._URL.format(urllib2.quote(query))
        self.createDirectory()
        self.initiateDownload()

    def __createDirName(self, query):
        dirName = query.lower().split()
        dirName = '_'.join(dirName)
        return dirName

    def __createDirPath(self):
        dirPath = self._BASE_DIR + self.dirName
        return dirPath

    def __createQuery(self, query):
        query = query.lower().split()
        query = '+'.join(query)
        return query

    def createDirectory(self):
        if not os.path.exists(self.dirPath):
            os.mkdir(self.dirPath)
        print("Creating directory: ", self.dirPath)

    def initiateDownload(self):
        imgList = []
        driver = self._DRIVER
        url = self.url
        driver.get(url)
        for _ in range(20):
            driver.execute_script("window.scrollBy(0,100)")
            try:
                driver.find_element_by_css_selector('.mye4qd').click()
            except:
                continue
        for i, src in enumerate(driver.find_elements_by_xpath('//img[contains(@class, "rg_i Q4LuWd")]')):
            try:
                src.click()
                img = driver.find_element_by_xpath("/html/body/div[2]/c-wiz/div[4]/div[2]/div[3]/div/div/div[3]/div[2]/c-wiz/div[1]/div[1]/div/div[2]/a/img").get_attribute("src")
                imgList.append(img)
                print("Image appended.")
            except:
                pass
        print("Writing image list to file.")
        print("{} of images collected for downloads".format(len(imgList)))
        self.writeImagesToFile(imgList)

    def writeImagesToFile(self, imgList):
        for i, src in enumerate(imgList):
            try:
                response = requests.get(src, stream=True)
                imgPath = os.path.join(self.dirPath + "/" + self.dirName + str(i) + '.jpg')
                with open(imgPath, 'wb') as file:
                    shutil.copyfileobj(response.raw, file)

                counter = len([i for i in os.listdir(self.dirPath)]) + 1
                print("Image nr " + str(counter) + " saved.")

            except Exception:
                pass



class ThumbnailDownloader(object):
    _URL = "https://www.google.co.in/search?q={}&source=lnms&tbm=isch"
    _BASE_DIR = PATH_FACES_REC
    _HEADERS = {
        'User-Agent': "Mozilla/5.0 (Windows NT 6.1; WOW64) "
                      "AppleWebKit/537.36 (KHTML, like Gecko) "
                      "Chrome/43.0.2357.134 "
                      "Safari/537.36"
    }

    def __init__(self):
        query = "marcin gortat"
        self.dirName = self.__createDirName(query)
        query = self.__createQuery(query)
        self.url = self._URL.format(urllib2.quote(query))
        self.createDirectory()
        self.initiateDownload()

    def __createDirName(self, query):
        query.lower()
        dirName = query.split()
        dirName = '_'.join(dirName)
        return dirName

    def __createQuery(self, query):
        query.lower()
        query = query.split()
        query = '+'.join(query)
        return query

    def __getSoup(self, url, header):
        return BeautifulSoup(urllib2.urlopen(urllib2.Request(url, headers=header)), 'html.parser')

    def createDirectory(self):
        if not os.path.exists(self.dirName):
            os.mkdir(self.dirName)
        print("Creating directory: ", self.dirName)

    def initiateDownload(self):
        imageList = []
        soup = self.__getSoup(self.url, self._HEADERS)
        print("Downloading images.")
        for img in soup.find_all('img'):
            if img.has_attr("data-src"):
                imageList.append(img['data-src'])
        print("Writing image list to file.")
        print ("{} of images collected for downloads".format(len(imageList)))
        self.writeImagesToFile(imageList)

    def writeImagesToFile(self, imageList):
        for i, src in enumerate(imageList):
            try:
                req = urllib2.Request(src, headers=self._HEADERS)
                raw_img = urllib2.urlopen(req).read()

                counter = len([i for i in os.listdir(self.dirName)]) + 1
                print("Image nr " + str(counter) + " saved.")
                with open(os.path.join(self.dirName, str(i)+".jpg"), 'wb') as file:
                    file.write(raw_img)
                    file.close()
            except Exception as e:
                print("Unable to load " + str(src))
                print(e)


class ScrapInputMenu:
    _FIELDS = {'Searching phrase: '}

    def __init__(self):
        self.fields = self._FIELDS
        self.root = tk.Tk()
        self.entries = self.__makeform(root=self.root)

    def __makeform(self, root, fields=None):
        if fields is None:
            fields = self._FIELDS
        entries = []
        for field in fields:
            row = tk.Frame(root)
            lab = tk.Label(row, width=15, text=field, anchor='w')
            ent = tk.Entry(row)
            row.pack(side=tk.TOP, fill=tk.X, padx=5, pady=5)
            lab.pack(side=tk.LEFT)
            ent.pack(side=tk.RIGHT, expand=tk.YES, fill=tk.X)
            entries.append((field, ent))
        return entries

    def __fetch(self):
        entries = self.entries
        for entry in entries:
            field = entry[0]
            text = entry[1].get()
            print('%s: "%s"' % (field, text))
            if text:
                ImageDownloader(query=text)
                self.root.destroy()

    def assign(self):
        root = self.root
        b1 = tk.Button(root, text='Search', command=self.__fetch)
        b1.pack(side=tk.LEFT, padx=5, pady=5)
        b2 = tk.Button(root, text='Close', command=root.destroy)
        b2.pack(side=tk.LEFT, padx=5, pady=5)
        root.mainloop()
