import cv2
import numpy as np

def drawEdge(img, landmarks, start, end, isClosed=False):
    points = []
    for i in range(start, end+1):
        point = [landmarks.part(i).file, landmarks.part(i).y]
        points.append(point)

    points = np.array(points, dtype=np.int32)
    #print(points)
    cv2.polylines(img, [points], isClosed, (255, 0, 0), thickness=3, lineType=cv2.LINE_8)

def renderDelaunayTrianglesAlternative(img, landmarkPoints):
    imgTemp = img.copy()
    points = np.array(landmarkPoints, dtype=np.int32)
    convexHull = cv2.convexHull(points)
    rect = cv2.boundingRect(convexHull)
    subdiv = cv2.Subdiv2D(rect)
    subdiv.insert(landmarkPoints)
    triangles = subdiv.getTriangleList()
    triangles = np.array(triangles, dtype=np.int32)

    index = 0
    for t in triangles:
        point1 = (t[0], t[1])
        point2 = (t[2], t[3])
        point3 = (t[4], t[5])

        cv2.line(imgTemp, point1, point2, (0, 0, 255), 2)
        cv2.line(imgTemp, point2, point3, (0, 0, 255), 2)
        cv2.line(imgTemp, point1, point3, (0, 0, 255), 2)
        print("Original delaunay, point set nr ", index)
        print(point1, point2, point3)
        index = index + 1
        cv2.imshow("Delaunay triangulation", imgTemp)
        cv2.waitKey(50)

    cv2.imshow("Delaunay triangulation", imgTemp)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def renderConvexHull(img, landmarkPoints, cvuiMode=True, showWindows=False):
    imgTemp = img.copy()
    points = np.array(landmarkPoints, np.int32)
    convexHull = cv2.convexHull(points)
    cv2.polylines(imgTemp, [convexHull], True, (255, 0, 0), 3)

    if showWindows:
        cv2.imshow('Convex hull', imgTemp)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    if cvuiMode:
        return imgTemp

def renderJaw(img, landmarks):
    drawEdge(img, landmarks, 0, 16)         #linia szczeki

def renderEyebrows(img, landmarks):
    drawEdge(img, landmarks, 17, 21)        #lewa brew
    drawEdge(img, landmarks, 22, 26)        #prawa brew

def renderNose(img, landmarks):
    drawEdge(img, landmarks, 27, 30)        #przegroda nosowa
    drawEdge(img, landmarks, 30, 35, True)  #dolna linia i czubek

def renderEyes(img, landmarks):
    drawEdge(img, landmarks, 36, 41, True)  #lewe oko
    drawEdge(img, landmarks, 42, 47, True)  #prawe oko

def renderMouth(img, landmarks):
    drawEdge(img, landmarks, 48, 59, True)  #gorna warga
    drawEdge(img, landmarks, 60, 67, True)  #dolna warga

def renderFaceLine(img, landmarks, cvuiMode=True, showWindows=False):
    #assert sprawdza czy dane twierdzenie jest prawdziwe. jesli nie jest - wyrzuci AssertionError
    imgTemp = img.copy()
    assert(landmarks.num_parts == 68)
    renderJaw(imgTemp, landmarks)
    renderEyebrows(imgTemp, landmarks)
    renderNose(imgTemp, landmarks)
    renderEyes(imgTemp, landmarks)
    renderMouth(imgTemp, landmarks)

    if showWindows:
        cv2.imshow("Landmarks detection - lines", imgTemp)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    if cvuiMode:
        return imgTemp

def renderFaceDots(img, landmarks, cvuiMode=True, showWindows=True):
    imgTemp = img.copy()
    assert(landmarks.num_parts == 68)
    for i in range(0, 68):
        x = landmarks.part(i).x
        y = landmarks.part(i).y

        cv2.circle(imgTemp, center=(x,y), radius=3, color=(0,255, 0), thickness=-1)

    if showWindows:
        cv2.imshow("Landmarks detection - dots", imgTemp)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    if cvuiMode:
        return imgTemp

def permeationAnimation(imgSource, imgTarget):
    alpha = 0
    imgMorph = imgSource.copy()

    while alpha < 1:
        imgMorph = (1 - alpha) * imgSource + alpha * imgTarget
        alpha += 0.025
        cv2.imshow("Output", imgMorph)
        cv2.waitKey(50)

    cv2.imshow("Output", imgMorph)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def x_renderConvexHullContours(img, convexHull):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(gray, 130, 255, cv2.THRESH_BINARY)
    contours, hierarchy = cv2.findContours(thresh, 2, 1)
    print(len(contours))
    cnt = contours[0]
    print(convexHull)
    defects = cv2.convexityDefects(cnt, convexHull)


    for i in range(defects.shape[0]):
        s,e,f,d = defects[i, 0]
        start = tuple(cnt[s][0])
        end = tuple(cnt[e][0])
        far = tuple(cnt[f][0])
        cv2.line(img, start, end, [0, 255, 0], 2)
        cv2.circle(img, far, 5, [0, 0, 255], -1)

    drawing = np.zeros((thresh.shape[0]))
    cv2.imshow("Grayscale image", gray)
    cv2.imshow("Thresholded image", thresh)
    cv2.imshow('Convex hull', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def x_renderDelaunayTriangles(img, landmarkPoints):
    points = np.array(landmarkPoints, dtype=np.int32)
    print("Delaunay points")
    print(points)
    cv2.polylines(img, [points], (255, 0, 0), thickness=2, lineType=cv2.LINE_8)

    cv2.imshow("Delaunay triangulation", img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
