import os,time
import sys
import dlib
import cv2
import numpy as np
import faceswap_common as fc
from path_constants import PATH_PREDICTOR, PATH_FACE_REC_MODEL, PATH_FACES_REC, PATH_ENROLL

def recognizeFaceMain(img):
    THRESHOLD = 0.5

    faceDetector = dlib.get_frontal_face_detector()
    shapePredictor = dlib.shape_predictor(PATH_PREDICTOR)
    faceRecognizer = dlib.face_recognition_model_v1(PATH_FACE_REC_MODEL)

    index = np.load(PATH_ENROLL + 'index.pkl', allow_pickle=True)
    faceDescriptorsEnrolled = np.load(PATH_ENROLL + 'descriptors.npy')

    # read image

    if len(sys.argv) > 1:
        imagePath = sys.argv[1]

    # exit if unable to read frame from feed
    if img is None:
        print('cannot read image')
        sys.exit(0)

    t = time.time()
    # detect faces in image
    faces = faceDetector(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))

    # Now process each face we found
    for face in faces:

        # Find facial landmarks for each detected face
        shape = shapePredictor(cv2.cvtColor(img, cv2.COLOR_BGR2RGB), face)

        # find coordinates of face rectangle
        x1, y1, x2, y2 = fc.findRectangle(face)

        faceDescriptor = faceRecognizer.compute_face_descriptor(img, shape)

        faceDescriptorNdarray = fc.convertDlibToArray(faceDescriptor)

        distances = np.linalg.norm(faceDescriptorsEnrolled - faceDescriptorNdarray, axis=1)

        argmin = np.argmin(distances)  # index
        minDistance = distances[argmin]  # minimum distance

        if minDistance <= THRESHOLD:
            label = index[argmin]
        else:
            label = 'unknown'

        print("time taken = {:.3f} seconds".format(time.time() - t))

        # Draw a rectangle for detected face
        cv2.rectangle(img, (x1, y1), (x2, y2), (0, 0, 255))

        # Draw circle for face recognition
        center, radius, color = fc.findCircle(x1, y1, x2, y2)
        cv2.circle(img, center, radius, color, thickness=1, lineType=8, shift=0)

        # Write test on image specifying identified person and minimum distance
        org = (int(x1), int(y1))  # bottom left corner of text string
        font_face = cv2.FONT_HERSHEY_SIMPLEX
        font_scale = 0.5
        text_color = (255, 0, 0)
        printLabel = '{} {:0.4f}'.format(label, minDistance)
        cv2.putText(img, printLabel, org, font_face, font_scale, text_color, thickness=2)
        print(label, minDistance)

    # Show result
    #cv2.imshow('facerec', img)
    #cv2.imwrite('result-dlib-{}.jpg'.format(label), im)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    return img