import shutil
import sys
import os
import dlib
import cv2
import numpy as np
import _pickle as pickle
import faceswap_common as fc
import data_methods as dm
from path_constants import PATH_PREDICTOR, PATH_FACE_REC_MODEL, PATH_FACES_REC, PATH_ENROLL, PATH_UNENROLLED

faceDetector = dlib.get_frontal_face_detector()
shapePredictor = dlib.shape_predictor(PATH_PREDICTOR)
faceRecognizer = dlib.face_recognition_model_v1(PATH_FACE_REC_MODEL)

# create subfolder list
subfolders = dm.createSubfolderList(PATH_FACES_REC)

# create label dictionary, return list of: image paths, labels
imagePaths, labels, nameLabelDict = dm.createLabelDictionary(subfolders)

index = {}
i = 0
faceDescriptors = None
for imagePath in imagePaths:
    print("processing: {}".format(imagePath))
    # read image and convert it to RGB
    img = cv2.imread(imagePath)

    # detect faces in image
    try:
        faces = faceDetector(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    except Exception:
        print("Unclassified image spotted. - faceDetector")
        pass

    print("{} Face(s) found".format(len(faces)))

    if len(faces) == 0:
        shutil.move(imagePath, PATH_UNENROLLED)
        print("Image moved to ", PATH_UNENROLLED)

    # Now process each face we found
    for k, face in enumerate(faces):

        # Find facial landmarks for each detected face
        try:
            shape = shapePredictor(cv2.cvtColor(img, cv2.COLOR_BGR2RGB), face)
            # convert landmarks from Dlib's format to list of (x, y) points
            landmarks = fc.convertLandmarksToPoints(shape)

            # Compute face descriptor using neural network defined in Dlib.
            # It is a 128D vector that describes the face in img identified by shape.
            faceDescriptor = faceRecognizer.compute_face_descriptor(img, shape)

            # Convert face descriptor from Dlib's format to list, then a NumPy array
            faceDescriptorNdarray = fc.convertDlibToArray(faceDescriptor)

            # Stack face descriptors (1x128) for each face in images, as rows
            if faceDescriptors is None:
                faceDescriptors = faceDescriptorNdarray
            else:
                faceDescriptors = np.concatenate((faceDescriptors, faceDescriptorNdarray), axis=0)

            # save the label for this face in index. We will use it later to identify
            # person name corresponding to face descriptors stored in NumPy Array
            index[i] = nameLabelDict[imagePath]
            i += 1
        except Exception:
            print("Unclassified image spotted. - shapePredictor")
            pass



np.save(PATH_ENROLL + 'descriptors.npy', faceDescriptors)

with open(PATH_ENROLL + 'index.pkl', 'wb') as f:
    pickle.dump(index, f)