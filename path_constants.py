#pojedyncza kropka - czyli zostaje dalej w tym samym pliku
#to sa tylko wartosci do zmiennych zeby nie musiec ich wpisywac na sztywno w wielu miejscach za kazdym razem

PATH_DATA = "./data/"
PATH_MODEL = "./data/models/"
PATH_IMAGES = "./data/images/faces_swap/"
PATH_FACES_REC = "./data/images/faces_recognition/"
PATH_SAVED = "./data/saved_img/"
PATH_PREDICTOR = "./data/models/shape_predictor_68_face_landmarks.dat"
PATH_FACE_REC_MODEL = "./data/models/dlib_face_recognition_resnet_model_v1.dat"
PATH_ENROLL = "./data/enroll/"
PATH_UNENROLLED = "./data/images/unenrolled/"

