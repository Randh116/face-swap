import dlib
import cv2
import numpy as np
import render as rd
from faceswap_emptyframe import EmptyFrame
import faceswap_image as fsim



def detectRectangle(img):

    font = cv2.FONT_HERSHEY_SIMPLEX

    frame = img
    blur = cv2.GaussianBlur(frame, (7, 7), 0.5)
    edge = cv2.Canny(blur, 0, 50, 3)

    contours, hierarchy = cv2.findContours(edge, cv2.RETR_TREE,
                                           cv2.CHAIN_APPROX_SIMPLE)

    for contour, hier in zip(contours, hierarchy):
        (x, y, w, h) = cv2.boundingRect(contour)
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
        cv2.putText(frame, ('width = {}, height = {}'.format(w, h)),
                    (x + 30, y + 30),
                    font,
                    1,
                    (0, 255, 0),
                    2,
                    cv2.LINE_AA)

    cv2.imshow('Measure Size', frame)
    cv2.waitKey(0)



def saveLandmarksToFile(landmarks, fileName):
    with open(fileName, 'w') as file:
        for point in landmarks.parts():
            file.write("%s %s\n" % (int(point.x), int(point.y)))
    file.close()

def convertLandmarksToPoints(shape):
  points = []
  for p in shape.parts():
    pt = (p.x, p.y)
    points.append(pt)
  return points

def convertLandmarksToPointsAlt(shape):
    landmarks_points = []
    for n in range(0, 68):
        x = shape.part(n).x
        y = shape.part(n).y
        landmarks_points.append((x, y))
    points = np.array(landmarks_points, np.int32)
    return points

def findFaceLandmarks(faceDetector, shapePredictor, img, render=False, cvuiMode=True):
    landmarkPoints = []
    imgRendered = EmptyFrame(img)
    detectedFaces = faceDetector(img, 0)
    print("Number of detected faces: ", len(detectedFaces))

    if len(detectedFaces) > 0:
        maxArea = 0
        maxFaceSize = None
        for face in detectedFaces:
            if face.area() > maxArea:
                maxArea = face.area()
                maxFaceSize = [face.left(),
                               face.top(),
                               face.right(),
                               face.bottom()]

        rect = dlib.rectangle(*maxFaceSize)
        landmarks = shapePredictor(img, rect)

        #print(landmarks)
        #print(landmarks.parts())

        print("Number of landmarks: ", len(landmarks.parts()))
        if render:
            if cvuiMode:
                imgRendered.zeros = rd.renderFaceDots(img, landmarks, showWindows=False)
            if not cvuiMode:
                rd.renderFaceDots(img, landmarks, cvuiMode=False, showWindows=False)
            #rd.renderFaceLine(img, landmarks)

        landmarkPoints = convertLandmarksToPoints(landmarks)
        #landmarkPoints = convertLandmarksToPointsAlt(landmarks)
        #print("landmarkPoints\n", landmarkPoints)

    if cvuiMode:
        return landmarkPoints, imgRendered
    else:
        return landmarkPoints

def findConvexHull(points1, points2):
    convexHull1 = []
    convexHull2 = []

    # wklada tylko tablice obrazu ktory widzimy, bo to co widzimy to target
    # na tej podstawie wyznacza indeksy, tzn to ktore landmarki tworza tego convexhulla
    # z tego powodu wiadomo ktore landmarki tworza tego convexhulla rowniez w drugiej twarzy
    # i na tej podstawie mozemy uzyc tylko jednego zestawu punktow

    print("points2\n", points2)
    points2_array = np.array(points2)
    print("points2_array\n", points2_array)

    # returnPoints=False jest odpowiedzialne za to ze zwracaja sie same pojedyncze liczby
    # zwraca indeksy odpowiadajace punktom w podanej tablicy
    # np pozycja 13 w tablicy to pierwszy punkt tworzacy
    # pozycja 10 to drugi itd

    index = cv2.convexHull(points2_array, returnPoints=False)
    print("convexhull", index)
    print("index convhull", index[0][0])

    # a tutaj odbywa sie budowanie listy 'prawdziwych' punktow
    # tzn jest wyciagane x oraz y na podstawie indexow landmarkow
    for i in range(0, len(index)):
        convexHull1.append(points1[index[i][0]])
        convexHull2.append(points2[index[i][0]])

    return convexHull1, convexHull2

def findConvexHullAlt(points1, points2):
    convexHull1 = cv2.convexHull(points1)
    convexHull2 = cv2.convexHull(points2)

    return convexHull1, convexHull2

def findImageSize(img):
    imgSize = img.shape
    rect = (0, 0, imgSize[1], imgSize[0])
    return rect

def containsRect(rect, point):
    if point[0] < rect[0]:
        return False
    elif point[1] < rect[1]:
        return False
    elif point[0] > rect[2]:
        return False
    elif point[1] > rect[3]:
        return False
    return True


"""
1. Create an instance of Subdiv2D - a structure that divides a face into triangles
2. Inserting points into subdiv
3. Getting delaunay triangulation - list of triangles
4. Finding indices of triangles in the points array
5. 
"""
def makeDelaunayTriangulation(rect, points, img, render=False, cvuiFrame=None):
    imgTemp = img.copy()
    subdiv = cv2.Subdiv2D(rect)

    #print("points", points)
    # for p in points:
    #   subdiv.insert((p[0], p[1]))

    subdiv.insert(points)

    triangles = subdiv.getTriangleList()
    #print("triangles", triangles)
    delaunayTriangles = []
    index = 0
    for t in triangles:
        pt = []
        # t[0], t[1] - (x,y) wierzcholek nr1
        # t[2], t[3] - (x,y) wierzcholek nr2
        # t[4], t[5] - (x,y) wierzcholek nr3
        # czyli sa trzy tablice zbierajace parametry wierzcholkow z osobna
        pt.append((t[0], t[1]))
        pt.append((t[2], t[3]))
        pt.append((t[4], t[5]))

        point1 = (t[0], t[1])
        point2 = (t[2], t[3])
        point3 = (t[4], t[5])

        if render:
            cv2.line(imgTemp, point1, point2, (0, 0, 255), 2)
            cv2.line(imgTemp, point2, point3, (0, 0, 255), 2)
            cv2.line(imgTemp, point1, point3, (0, 0, 255), 2)
            print("Altered delaunay, point set nr ", index)
            print(point1, point2, point3)
            if cvuiFrame is None:
                cv2.imshow("Delaunay triangulation", imgTemp)
                cv2.waitKey(100)
            if cvuiFrame is not None:
                fsim.renderFrameCvui(cvuiFrame, imgTemp, showTime=100)
        index = index + 1

        # ?????
        # sprawdzenie czy dany trojkat znajduje sie w obrazie poprzez sprawdzenie
        # czy wszystkie 3 wierzcholki sa w jego obrebie
        # pozniej sa szukane te 3 wierzcholki
        # porownujac x,y wierzcholkow trojkatow oraz x,y punktow convexhulla
        # i wyszukuje odpowiednie punkty x,y w convexhullu ktore sa odpowiednie na stworzenie trojkata
        # poniewaz wierzcholki wskazuja te odpowiednie punkty
        # i po prostu sprawdza czy te punkty sa oddalone o mniej niz 1.0 wartosciowo od siebie
        # bo to pomaga skutecznie to przefiltrowac i wykluczyc wiele punktow
        if containsRect(rect, point1) and containsRect(rect, point2) and containsRect(rect, point3):
            indices = []
            c = 0
            for i in range(0, 3):
                for j in range(0, len(points)):
                    # pt[i][0] - parametr x 1-szego wierzcholka
                    # pt[i][1] - parametr y 1-szego wierzcholka
                    # przeksztalca punkty na indeksy
                    print("Filtrowanie punktow")
                    print(pt[i][0])
                    print(points[j][0])
                    print("roznica", abs(pt[i][0] - points[j][0]))
                    print(pt[i][1])
                    print(points[j][1])
                    print("roznica", abs(pt[i][1] - points[j][1]))

                    if(abs(pt[i][0] - points[j][0]) < 1.0
                    and abs(pt[i][1] - points[j][1]) < 1.0):

                        # syf syf syf syf
                        c += 1
                        print("roznica v2 0 ", abs(pt[i][0] - points[j][0]))
                        print("roznica v2 1 ", abs(pt[i][1] - points[j][1]))
                        print("wykonano ", c)
                        # print(j)
                        # syf syf syf syf

                        indices.append(j)
            if len(indices) == 3:
                delaunayTriangles.append((indices[0], indices[1], indices[2]))

    if render and cvuiFrame is None:
        cv2.imshow("Delaunay triangulation", imgTemp)
        cv2.waitKey(500)
    if cvuiFrame is not None:
        cv2.waitKey(3000)
        #cv2.destroyAllWindows()

    return delaunayTriangles

def extractIndex(nparray):
    index = None
    for num in nparray[0]:
        index = num
        break
    return index

def makeDelaunayTriangulationAlt(landmarkPoints, img, render=False):
    imgTemp = img.copy()
    points = np.array(landmarkPoints, dtype=np.int32)
    convexHull = cv2.convexHull(points)
    rect = cv2.boundingRect(convexHull)
    subdiv = cv2.Subdiv2D(rect)
    subdiv.insert(landmarkPoints)
    triangles = subdiv.getTriangleList()
    triangles = np.array(triangles, dtype=np.int32)

    index = 0
    delaunayTriangles = []
    for t in triangles:
        point1 = (t[0], t[1])
        point2 = (t[2], t[3])
        point3 = (t[4], t[5])

        index_pt1 = np.where((points == point1).all(axis=1))
        index_pt1 = extractIndex(index_pt1)

        index_pt2 = np.where((points == point2).all(axis=1))
        index_pt2 = extractIndex(index_pt2)

        index_pt3 = np.where((points == point3).all(axis=1))
        index_pt3 = extractIndex(index_pt3)

        if render:
            cv2.line(imgTemp, point1, point2, (0, 0, 255), 2)
            cv2.line(imgTemp, point2, point3, (0, 0, 255), 2)
            cv2.line(imgTemp, point1, point3, (0, 0, 255), 2)
            print("Original delaunay, point set nr ", index)
            print(point1, point2, point3)
            index = index + 1
            cv2.imshow("Delaunay triangulation", imgTemp)
            cv2.waitKey(50)

        if index_pt1 is not None and index_pt2 is not None and index_pt3 is not None:
            triangle = [index_pt1, index_pt2, index_pt3]
            delaunayTriangles.append(triangle)

    cv2.imshow("Delaunay triangulation", imgTemp)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return delaunayTriangles

def warpTriangle(img1, img2, triangle1, triangle2):
    rect1 = cv2.boundingRect(np.float32([triangle1]))
    rect2 = cv2.boundingRect(np.float32([triangle2]))

    img1Crop = img1[rect1[1]:rect1[1]+rect1[3], rect1[0]:rect1[0]+rect1[2]]

    triangle1Crop = []
    triangle2Crop = []

    for i in range(0, 3):
        triangle1Crop.append(((triangle1[i][0] - rect1[0]), (triangle1[i][1] - rect1[1])))
        triangle2Crop.append(((triangle2[i][0] - rect2[0]), (triangle2[i][1] - rect2[1])))

    warpMat = cv2.getAffineTransform(np.float32(triangle1Crop), np.float32(triangle2Crop))
    warpShape = (rect2[2], rect2[3])

    img2Crop = cv2.warpAffine(img1Crop, warpMat, warpShape, None, flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_REFLECT_101)

    # ustal maske i na jej podstawie wypelnij ten trojkat
    # maska wypelniona zerami o dlugosci rect2[3] i wysokosci rect2[2]
    mask = np.zeros((rect2[3], rect2[2], 3), dtype=np.float32)
    cv2.fillConvexPoly(mask, np.int32(triangle2Crop), (1,1,1), 16, 0)

    img2Crop = img2Crop * mask

    # tutaj nastepuje nadpisanie trojkatow na oryginalnym obrazie przez te zmodyfikowane
    # wytnij drugi wierzcholek trojkata az do drugiego wierzcholka + czwartego wierzcholka
    img2[rect2[1]:rect2[1] + rect2[3], rect2[0]:rect2[0] + rect2[2]] = \
        img2[rect2[1]:rect2[1] + rect2[3], rect2[0]:rect2[0] + rect2[2]] * (
                (1.0, 1.0, 1.0) - mask)

    img2[rect2[1]:rect2[1] + rect2[3], rect2[0]:rect2[0] + rect2[2]] = \
        img2[rect2[1]:rect2[1] + rect2[3], rect2[0]:rect2[0] + rect2[2]] + img2Crop

def findCenter(convexHull):
    rect = cv2.boundingRect(np.float32([convexHull]))
    # liczy srodek podanego kwadratu
    # punkt lewy gorny rog + polowa wysokosci, punkt prawy dolny rog + polowa szerokosci
    center = ((rect[0] + int(rect[2] / 2), rect[1] + int(rect[3] / 2)))
    return center

def findMask(img):
    mask = np.zeros(img.shape, dtype=img.dtype)
    return mask

def findRectangle(face):
    x1 = face.left()
    y1 = face.top()
    x2 = face.right()
    y2 = face.bottom()
    return x1, y1, x2, y2

def convertDlibToArray(faceDescriptor):
    faceDescriptorList = [m for m in faceDescriptor]
    faceDescriptorNdarray = np.asarray(faceDescriptorList, dtype=np.float64)
    faceDescriptorNdarray = faceDescriptorNdarray[np.newaxis, :]
    return faceDescriptorNdarray

def findCircle(x1, y1, x2, y2):
    center = (int((x1 + x2)/2.0), int((y1 + y2)/2.0))
    radius = int((y2-y1)/2.0)
    color = (0, 255, 0)
    return center, radius, color


