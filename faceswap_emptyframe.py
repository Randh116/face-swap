import numpy as np

class EmptyFrame(object):
    def __init__(self, img):
        h, w, c = img.shape
        self.height = h
        self.width = w
        self.channels = c
        frame = np.zeros((self.height, self.width, self.channels), np.uint8)
        self.zeros = frame

    # jak to dodamy, to mozemy korzystac z obiektu lub jego wlasciwosci
    # w podobny sposob jak z listy
    # wtedy obiekt jest 'subscriptable' i mozemy wyciagac wartosci pojedynczo z nawiasow
    # np. emptyFrame.zeros[:]

    def __getitem__(self, item):
        return self.zeros[item]

