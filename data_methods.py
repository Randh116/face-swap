import cv2
import os
import random
from path_constants import PATH_IMAGES, PATH_FACES_REC

def deliverDataSource():
    imgName = "donald_trump.jpg"
    imgPath = PATH_IMAGES + imgName
    img = cv2.imread(imgPath)
    return img

def deliverDataTarget():
    imgName = "hillary_clinton.jpg"
    imgPath = PATH_IMAGES + imgName
    img = cv2.imread(imgPath)
    return img

def deliverDataRecognition():
    imgName = "duda_demo.jpg"
    imgPath = PATH_FACES_REC + imgName
    img = cv2.imread(imgPath)
    return img

def createSubfolderList(dirPath):
    subfolders = []
    for file in os.listdir(dirPath):
        fullPath = os.path.join(dirPath, file)
        if os.path.isdir(fullPath):
            subfolders.append(fullPath)
    return subfolders

def createLabelDictionary(subfolderList):
    nameLabelDict = {}
    labels = []
    imagePaths = []
    for i, subfolder in enumerate(subfolderList):
        for file in os.listdir(subfolder):
            fullPath = os.path.join(subfolder, file)
            if file.endswith('jpg'):
                imagePaths.append(fullPath)
                labels.append(i)
                nameLabelDict[fullPath] = subfolder.split('/')[-1]
    return imagePaths, labels, nameLabelDict

def loadRandomFace(dirPath):
    files = []
    subfolders = createSubfolderList(dirPath)
    randomSubfolder = random.choice(subfolders)
    for file in os.listdir(randomSubfolder):
        fullPath = os.path.join(randomSubfolder, file)
        if os.path.isfile(fullPath):
            files.append(fullPath)
    randomFile = random.choice(files)
    img = cv2.imread(randomFile, cv2.IMREAD_COLOR)
    return img



