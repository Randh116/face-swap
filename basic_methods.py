import cv2
import numpy as np
import pytesseract


# Oryginalne zdjęcie
def original_img(img_name):
    img_n = cv2.imread(img_name, 1)
    return img_n

# Negatyw
def inverted(img_i):
    inverted_img = 255 - img_i
    return inverted_img

# Konwersja do odcieni szarości
def gray(img_g):
    gray_img = cv2.cvtColor(img_g, cv2.COLOR_BGR2GRAY)
    return gray_img

# Normalizacja histogramu
def histogram(img_h):
    hist_img = cv2.cvtColor(img_h, cv2.COLOR_BGR2GRAY)
    hist_img2 = cv2.equalizeHist(hist_img)
    return hist_img2

# Skalowanie
def scale_up(img_su):
    scale_up_img = cv2.resize(img_su, None, fx=2, fy=2)
    return scale_up_img

def scale_down(img_sd):
    scale_down_img = cv2.resize(img_sd, None, fx=0.5, fy=0.5)
    return scale_down_img

# Progowanie
def thresholding(img_tg):
    img_tg = cv2.medianBlur(img_tg, 5)

    #ret, thresh_img = cv2.threshold(img_tg, 127, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C)
    #thresh_img = cv2.adaptiveThreshold(img_tg, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 11, 2)
    thresh_img = cv2.adaptiveThreshold(img_tg, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 7)

    return thresh_img

# Filtry
# Rozmycie Gaussa
def gaussian(img_ga):
    gaussian_img = cv2.GaussianBlur(img_ga, (7, 7), 0)
    return gaussian_img

# Sepia
def sepia(img_sep):
    sepia_img = cv2.transform(img_sep, np.matrix([[0.272, 0.534, 0.131],
                                                  [0.349, 0.686, 0.168],
                                                  [0.393, 0.769, 0.189]]))
    return sepia_img

# Szkic ołówkiem
def pencil_sketch(img_ps):
    pencil_sketch_gray, _ = cv2.pencilSketch(img_ps, sigma_s=60, sigma_r=0.07, shade_factor=0.05)
    return pencil_sketch_gray

# Transformacja pomiędzy przestrzeniami barw
def convert(img_conv):
    hsv = cv2.cvtColor(img_conv, cv2.COLOR_RGB2HSV)
    return hsv

# Obrót
def rotate90(img_r):
    rot90 = cv2.rotate(img_r, cv2.ROTATE_90_CLOCKWISE)
    return rot90

def rotate180(img_r):
    rot180 = cv2.rotate(img_r, cv2.ROTATE_180)
    return rot180

# Zmiana jasności
def brightness_change(img_br, value):
    hsv = cv2.cvtColor(img_br, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    lim = 255 - value
    v[v > lim] = 255
    v[v <= lim] += value

    final_hsv = cv2.merge((h, s, v))
    img_br = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    return img_br

# Detekcja krawędzi
def edge_detection(img_ed):
    edges = cv2.Canny(img_ed, 100, 300)
    return edges

# Segmentacja / zepsute
def segmentation(img_seg):
    return img_seg

# Szkieletyzacja
def skeletonization(skeletonization_img):
    skeletonization_img = cv2.cvtColor(skeletonization_img, cv2.COLOR_BGR2GRAY)
    ret, skeletonization_img = cv2.threshold(skeletonization_img, 127, 255, 0)
    skel = np.zeros(skeletonization_img.shape, np.uint8)

    element = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))

    while True:
        open_s = cv2.morphologyEx(skeletonization_img, cv2.MORPH_OPEN, element)
        temp = cv2.subtract(skeletonization_img, open_s)
        eroded = cv2.erode(skeletonization_img, element)
        skel = cv2.bitwise_or(skel, temp)
        skeletonization_img = eroded.copy()

        if cv2.countNonZero(skeletonization_img) == 0:
            break

    return skel

# Erozja
def erosion(img_e):
    img_ero = cv2.cvtColor(img_e, cv2.COLOR_BGR2GRAY)
    kernel = np.ones((5, 5), np.uint8)
    img_erosion = cv2.erode(img_ero, kernel, iterations=1)
    return img_erosion

# Dylatacja
def dilation(img_d):
    img_dil = cv2.cvtColor(img_d, cv2.COLOR_BGR2GRAY)
    kernel = np.ones((5, 5), np.uint8)
    img_dilation = cv2.dilate(img_dil, kernel, iterations=1)
    return img_dilation

# Implementacja algorytmu OCR
def ocr(img_ocr):
    pytesseract.pytesseract.tesseract_cmd = 'D:\\Program Files\\Tesseract-OCR\\tesseract.exe'

    gray_ocr = cv2.cvtColor(img_ocr, cv2.COLOR_BGR2GRAY)

    ret, thresh1 = cv2.threshold(gray_ocr, 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY_INV)
    rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (50, 50))
    dilation_ocr = cv2.dilate(thresh1, rect_kernel, iterations=1)

    contours, hierarchy = cv2.findContours(dilation_ocr, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    img2 = img_ocr.copy()

    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        cropped = img2[y:y + h, x:x + w]
        text = pytesseract.image_to_string(cropped)
        print(text)

