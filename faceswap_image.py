import dlib
import cv2
import numpy as np
from py._builtin import execfile

import faceswap_common as fc
import render as rd
import basic_methods as bm
import data_methods as dm
import cvui
from path_constants import PATH_PREDICTOR, PATH_SAVED, PATH_FACES_REC
import os
import tkinter as tk

from faceswap_emptyframe import EmptyFrame
from faceswap_recognition_image import recognizeFaceMain
from face_image_scraping import ScrapInputMenu

GUI_BASIC_NAME = 'FaceSwap - Basic manipulation'
GUI_BASIC_MENU_NAME = 'BASIC - SETTINGS'
GUI_SWAP_NAME = 'FaceSwap - Steps visualization'
GUI_SWAP_MENU_NAME = 'STEPS VIS - SETTINGS'
GUI_RECOGNITION_NAME = 'FaceRecognition - Classification'
GUI_RECOGNITION_MENU_NAME = 'FACE RECG - SETTINGS'
GUI_MODE_NAME = 'Choose mode'

renderDict = {
    'landmarks':        False,
    'hull':             False,
    'triangulation':    False,
    'warping':          False,
    'cloning':          False
}


def showDictInfo(dict):
    print("Methods to show: %s" % list(dict.items()))
    print("Length: %d" % len(dict))
    print("Variable type: %s" % type(dict))
    print("Variable type: %s" % type(dict['landmarks']))


def setRender(dictElemName, boolVar):
    # dictElemName = '\'' + dictElemName + '\''
    renderDict.update({dictElemName: boolVar})


def updateRenderInfo(checklist):
    print(checklist[0][0])
    if checklist[0][0] is True:
        setRender('landmarks', True)
    if not checklist[0][0]:
        setRender('landmarks', False)
    if checklist[1][0] is True:
        setRender('hull', True)
    if not checklist[1][0]:
        setRender('hull', False)
    if checklist[2][0] is True:
        setRender('triangulation', True)
    if not checklist[2][0]:
        setRender('triangulation', False)
    if checklist[3][0] is True:
        setRender('warping', True)
    if not checklist[3][0]:
        setRender('warping', False)
    if checklist[4][0] is True:
        setRender('cloning', True)
    if not checklist[4][0]:
        setRender('cloning', False)


def menuMain():
    frame = np.zeros((230, 220, 3), np.uint8)
    quit = False
    cvui.init(GUI_MODE_NAME)
    while True and not quit:
        cvui.window(frame, 0, 0, 220, 20, "Settings")

        if cvui.button(frame, 10, 30, 200, 30, "BASIC MANIPULATION MODE"):
            quit = True
            cv2.destroyAllWindows()
            menuBasicMethods()
        if cvui.button(frame, 10, 70, 200, 30, "FACE SWAPPING MODE"):
            quit = True
            cv2.destroyAllWindows()
            menuFaceSwap()
        if cvui.button(frame, 10, 110, 200, 30, "FACE RECOGNITION MODE"):
            quit = True
            cv2.destroyAllWindows()
            menuRecognition()

        if not quit:
            cvui.imshow(GUI_MODE_NAME, frame)

        if cv2.waitKey(20) == 27:
            cv2.destroyAllWindows()
            break


def menuBasicMethods():
    img_src = dm.deliverDataSource()
    img = img_src.copy()

    h, w, c = img.shape
    if (h * 2 < 800 or w * 2 < 400):
        img = bm.scale_up(img)
        h = np.uint(h * 2)
        w = np.uint(w * 2)
    frame = np.zeros((h, w, c), np.uint8)

    frame[:] = img[:]
    frame_menu = np.zeros((590, 230, 3), np.uint8)
    quit = False

    cvui.init(GUI_BASIC_NAME)
    cvui.init(GUI_BASIC_MENU_NAME)

    imgNumber = 0
    while True and not quit:

        cvui.window(frame_menu, 0, 0, 220, 20, "Settings")

        if cvui.button(frame_menu, 10, 30, 100, 30, "Inverted colors"):
            img = bm.inverted(img)
            frame = np.zeros(img.shape, np.uint8)
            frame[:] = img[:]

        if cvui.button(frame_menu, 10, 70, 100, 30, "Gray colors"):
            img_tmp = bm.gray(img)
            frame = np.zeros((h, w), np.uint8)
            frame[:] = img_tmp[:]

        if cvui.button(frame_menu, 10, 110, 100, 30, "Normalization"):
            img_tmp = bm.histogram(img)
            frame = np.zeros((h, w), np.uint8)
            frame[:] = img_tmp[:]

        if cvui.button(frame_menu, 10, 150, 100, 30, "Scale up"):
            h, w, c = img.shape
            if h*2 < 2600 or w*2 < 2000:
                img = bm.scale_up(img)
                h = np.uint(h*2)
                w = np.uint(w*2)
            frame = np.zeros((h, w, c), np.uint8)
            frame[:] = img[:]

        if cvui.button(frame_menu, 10, 190, 100, 30, "Scale down"):
            h, w, c = img.shape
            if h/2 > 200 or w/2 > 150:
                img = bm.scale_down(img)
                h = np.uint(h/2)
                w = np.uint(w/2)
            frame = np.zeros((h, w, c), np.uint8)
            frame[:] = img[:]

        if cvui.button(frame_menu, 10, 230, 100, 30, "Threshholding"):
            img_tmp = bm.gray(img)
            img_thresh = bm.thresholding(img_tmp)
            frame = np.zeros((h, w), np.uint8)
            frame[:] = img_thresh[:]

        if cvui.button(frame_menu, 10, 270, 100, 30, "Gaussian"):
            img = bm.gaussian(img)
            frame = np.zeros((h, w, c), np.uint8)
            frame[:] = img[:]

        if cvui.button(frame_menu, 10, 310, 100, 30, "Sepia"):
            img = bm.sepia(img)
            frame = np.zeros((h, w, c), np.uint8)
            frame[:] = img[:]

        if cvui.button(frame_menu, 10, 350, 100, 30, "Pencil sketch"):
            img_tmp = bm.pencil_sketch(img)
            frame = np.zeros((h, w), np.uint8)
            frame[:] = img_tmp[:]

        if cvui.button(frame_menu, 120, 30, 100, 30, "Convert to hsv"):
            img = bm.convert(img)
            frame = np.zeros((h, w, c), np.uint8)
            frame[:] = img[:]

        if cvui.button(frame_menu, 120, 70, 100, 30, "Rotate 90"):
            img = bm.rotate90(img)
            w_tmp = w
            w = h
            h = w_tmp
            frame = np.zeros((h, w, c), np.uint8)
            frame[:] = img[:]

        if cvui.button(frame_menu, 120, 110, 100, 30, "Rotate 180"):
            img = bm.rotate180(img)
            frame = np.zeros((h, w, c), np.uint8)
            frame[:] = img[:]

        if cvui.button(frame_menu, 120, 150, 100, 30, "Brightness"):
            img = bm.brightness_change(img, 50)
            frame = np.zeros((h, w, c), np.uint8)
            frame[:] = img[:]

        if cvui.button(frame_menu, 120, 190, 100, 30, "Edge detection"):
            img_tmp = bm.edge_detection(img)
            frame = np.zeros((h, w), np.uint8)
            frame[:] = img_tmp[:]

        if cvui.button(frame_menu, 120, 230, 100, 30, "Skeletonization"):
            img_tmp = bm.skeletonization(img)
            frame = np.zeros((h, w), np.uint8)
            frame[:] = img_tmp[:]

        if cvui.button(frame_menu, 120, 270, 100, 30, "Erosion"):
            img_tmp = bm.erosion(img)
            frame = np.zeros((h, w), np.uint8)
            frame[:] = img_tmp[:]

        if cvui.button(frame_menu, 120, 310, 100, 30, "Dilation"):
            img_tmp = bm.dilation(img)
            frame = np.zeros((h, w), np.uint8)
            frame[:] = img_tmp[:]

        if cvui.button(frame_menu, 120, 350, 100, 30, "OCR"):
            bm.ocr(img)

        if cvui.button(frame_menu, 10, 390, 210, 30, "RESET"):
            img = img_src
            frame = np.zeros(img.shape, np.uint8)
            frame[:] = img[:]

        if cvui.button(frame_menu, 10, 430, 210, 30, "SAVE TO FILE"):
            os.chdir(PATH_SAVED)
            cv2.imwrite('image_saved_' + str(imgNumber) + '.png', img)
            print("Image successfully saved.")

        if cvui.button(frame_menu, 10, 470, 210, 30, "QUIT"):
            quit = True
            cv2.destroyAllWindows()

        if cvui.button(frame_menu, 10, 510, 210, 30, "BACK TO MAIN MENU"):
            quit = True
            cv2.destroyAllWindows()
            menuMain()

        # if cvui.button(frame_menu, 10, 550, 210, 30, "detect"):
        #     img_tmp = bm.gray(img)
        #     #img_thresh = bm.thresholding(img_tmp)
        #     #fc.detectRectangle(img_thresh)
        #     fc.detectRectangle(img_tmp)

        if not quit:
            cvui.imshow(GUI_BASIC_MENU_NAME, frame_menu)
            cvui.imshow(GUI_BASIC_NAME, frame)

        if cv2.waitKey(20) == 27:
            cv2.destroyAllWindows()
            break


def menuFaceSwap():
    img_src = dm.deliverDataTarget()
    img = img_src.copy()

    h, w, c = img.shape
    if (h * 2 < 800 or w * 2 < 400):
        img = bm.scale_up(img)
        h = np.uint(h * 2)
        w = np.uint(w * 2)
    frame = np.zeros((h, w, c), np.uint8)

    frame[:] = img[:]
    frame_menu = np.zeros((430, 230, 3), np.uint8)
    quit = False

    cvui.init(GUI_SWAP_NAME)
    cvui.init(GUI_SWAP_MENU_NAME)

    checked = [[False], [False], [False], [False], [False]]

    while True and not quit:

        cvui.window(frame_menu, 0, 0, 220, 20, "Settings")

        cvui.checkbox(frame_menu, 10, 30, "FACIAL LANDMARKS", checked[0])
        cvui.checkbox(frame_menu, 10, 70, "CONVEX HULL", checked[1])
        cvui.checkbox(frame_menu, 10, 110, "DELAUNAY TRIANGULATION", checked[2])
        cvui.checkbox(frame_menu, 10, 150, "TRIANGLE WARPING", checked[3])
        cvui.checkbox(frame_menu, 10, 190, "SEAMLESS CLONING", checked[4])

        if cvui.button(frame_menu, 10, 270, 210, 30, "START"):
            updateRenderInfo(checked)
            showDictInfo(renderDict)
            swapCore(frame)

        if cvui.button(frame_menu, 10, 310, 210, 30, "RESET"):
            img = img_src
            frame = np.zeros(img.shape, np.uint8)
            frame[:] = img[:]

        if cvui.button(frame_menu, 10, 350, 210, 30, "QUIT"):
            quit = True
            cv2.destroyAllWindows()

        if cvui.button(frame_menu, 10, 390, 210, 30, "BACK TO MAIN MENU"):
            quit = True
            cv2.destroyAllWindows()
            menuMain()

        if not quit:
            cvui.imshow(GUI_SWAP_MENU_NAME, frame_menu)
            cvui.imshow(GUI_SWAP_NAME, frame)

        if cv2.waitKey(20) == 27:
            cv2.destroyAllWindows()
            break

def menuRecognition():
    img_src = dm.deliverDataRecognition()
    img = img_src.copy()

    h, w, c = img.shape
    if (h * 2 < 800 or w * 2 < 400):
        img = bm.scale_up(img)
        h = np.uint(h * 2)
        w = np.uint(w * 2)
    frame = np.zeros((h, w, c), np.uint8)

    frame[:] = img[:]
    frame_menu = np.zeros((550, 230, 3), np.uint8)
    quit = False

    cvui.init(GUI_RECOGNITION_NAME)
    cvui.init(GUI_RECOGNITION_MENU_NAME)

    while True and not quit:

        cvui.window(frame_menu, 0, 0, 220, 20, "Settings")

        if cvui.button(frame_menu, 10, 30, 210, 30, "ENROLLMENT"):
            execfile('faceswap_recognition_enroll.py')

        if cvui.button(frame_menu, 10, 70, 210, 30, "INFERENCE - IMAGE"):
            img_tmp = recognizeFaceMain(img)
            frame = np.zeros(img_tmp.shape, np.uint8)
            frame[:] = img_tmp[:]

        if cvui.button(frame_menu, 10, 110, 210, 30, "INFERENCE - VIDEO"):
            pass

        if cvui.button(frame_menu, 10, 150, 210, 30, "LOAD RANDOM IMAGE"):
            img = dm.loadRandomFace(PATH_FACES_REC)
            frame = np.zeros(img.shape, np.uint8)
            frame[:] = img[:]

        if cvui.button(frame_menu, 10, 190, 210, 30, "DOWNLOAD IMAGES FROM GOOGLE"):
            menuInstance = ScrapInputMenu()
            menuInstance.assign()
            print("Downloading completed. Please enroll data again to recognize new faces.")

        if cvui.button(frame_menu, 10, 230, 210, 30, "RESET"):
            img = img_src
            frame = np.zeros(img.shape, np.uint8)
            frame[:] = img[:]

        if cvui.button(frame_menu, 10, 270, 210, 30, "QUIT"):
            quit = True
            cv2.destroyAllWindows()

        if cvui.button(frame_menu, 10, 310, 210, 30, "BACK TO MAIN MENU"):
            quit = True
            cv2.destroyAllWindows()
            menuMain()

        if not quit:
            cvui.imshow(GUI_RECOGNITION_MENU_NAME, frame_menu)
            cvui.imshow(GUI_RECOGNITION_NAME, frame)

        if cv2.waitKey(20) == 27:
            cv2.destroyAllWindows()
            break

def renderFrameCvui(frame, content, showTime=3000):
    frame[:] = content[:]
    cvui.imshow(GUI_SWAP_NAME, frame)
    cv2.waitKey(showTime)


def swapCore(frame):
    imgSource = dm.deliverDataSource()
    imgTarget = dm.deliverDataTarget()

    srcRendered = EmptyFrame(imgSource)
    dstRendered = EmptyFrame(imgTarget)

    imgSourceWarped = np.copy(imgTarget)

    faceDetector = dlib.get_frontal_face_detector()
    shapePredictor = dlib.shape_predictor(PATH_PREDICTOR)

    landmarkPointsSource = fc.findFaceLandmarks(faceDetector, shapePredictor, imgSource,
                                                renderDict['landmarks'], cvuiMode=False)
    landmarkPointsTarget, dstRendered.zeros = fc.findFaceLandmarks(faceDetector, shapePredictor, imgTarget,
                                                                   renderDict['landmarks'])

    if renderDict['landmarks']:
        renderFrameCvui(frame, dstRendered.zeros)

    convexHullSource, convexHullTarget = fc.findConvexHull(landmarkPointsSource, landmarkPointsTarget)

    if renderDict['hull']:
        rd.renderConvexHull(imgSource, landmarkPointsSource, cvuiMode=False)
        dstRendered.zeros = rd.renderConvexHull(imgTarget, landmarkPointsTarget)
        renderFrameCvui(frame, dstRendered.zeros)

    rect = fc.findImageSize(imgTarget)

    delaunayTriangles = fc.makeDelaunayTriangulation(rect, convexHullTarget, imgTarget, renderDict['triangulation'], cvuiFrame=frame)

    # delaunayTriangles = fc.makeDelaunayTriangulationAlt(landmarkPointsTarget, imgTarget, render=True)
    # rd.renderDelaunayTrianglesAlternative(imgSource, landmarkPointsSource)
    # rd.renderDelaunayTrianglesAlternative(imgTarget, landmarkPointsTarget)

    if len(delaunayTriangles) == 0:
        quit()

    for i in range(0, len(delaunayTriangles)):
        tri_1 = []
        tri_2 = []

        for j in range(0, 3):
            tri_1.append(convexHullSource[delaunayTriangles[i][j]])
            tri_2.append(convexHullTarget[delaunayTriangles[i][j]])

        fc.warpTriangle(imgSource, imgSourceWarped, tri_1, tri_2)
        if renderDict['warping']:
            renderFrameCvui(frame, imgSourceWarped, showTime=100)

    if renderDict['warping']:
        cv2.waitKey(3000)

    cloningMask = fc.findMask(imgTarget)
    center = fc.findCenter(convexHullTarget)
    cv2.fillConvexPoly(cloningMask, np.int32(convexHullTarget), (255, 255, 255))

    output = cv2.seamlessClone(imgSourceWarped, imgTarget, cloningMask, center, cv2.NORMAL_CLONE)

    if renderDict['cloning']:
        renderFrameCvui(frame, output)

    # nieudana proba implementacji przenikania seamless clone
    # alpha = 0
    # imgTest = (1 - alpha) * imgSourceWarped + alpha * output
    # cv2.imshow("control", imgTest)

if __name__ == "__main__":
    menuMain()

